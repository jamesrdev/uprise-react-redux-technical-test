import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { IconContext } from 'react-icons';
import { FaCheckCircle } from 'react-icons/fa';
import './submit.scss';
import {
  Container,
  Row,
  Col,
  DropdownItem,
} from 'reactstrap';

import logical from './logic';
import validate from './validate';
import selectPlayer from './selectPlayer';
import displayProps from './displayProps';
import getValues from './getValues';
import makeInputs from './makeInputs';
import { getPlayersActionCreator, sendFormObjectActionCreator } from '../../redux/actions/actions';

let inputValues;
const myTeams = [null, 'Team 1', 'Team 2'];
const normaliseRadius = {
  borderBottomLeftRadius: '0px',
  borderBottomRightRadius: '0px',
};
const displaySuccess = {
  visibility: 'visible',
  opacity: '1',
};
let safe;

class Submit extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dropdownOpen: [false, false, false, false, false, false],
      playerInput: ['Enter player name', 'Enter player name', 'Enter opponent name', 'Enter opponent name', 'Select winning team'],
      mode: 'VS',
      error: '',
      safe: false,
      iconConfig: {
        className: 'submit__icons',
        style: {
          strokeWidth: '20px', stroke: '#28a745', width: '2.5em', height: '2.5em', fill: 'none', marginTop: '10px',
        },
      },
      success: false,
    };
  }

  componentWillMount() {
    this.props.getPlayersActionCreator();
  }

  componentDidMount() {
    this.validateFix();
  }

  componentDidUpdate() {
    if (this.state.error !== '') {
      // unsafe to send form
      safe = false;
    } else {
      // safe to send form
      safe = true;
    }
  }

  toggleDropDown = (index) => {
    const prevState = this.state.dropdownOpen;
    prevState[index] = !prevState[index];
    this.setState({
      dropdownOpen: [...prevState],
    }, () => {
      // console.log(this.state.dropdownOpen)
    });
  }

  logicalFix = logical()
  validateFix = validate()
  selectPlayer = selectPlayer()
  displayProps = displayProps()
  getValues = getValues()
  makeInputs = makeInputs()


  mapPlayers = index => this.props.playersArrayProp.map((v, i) => (
    <DropdownItem key={`player${i + 1}${index}`} onClick={() => this.selectPlayer(`selectPlayer${i + 1}${index}`, index)} ref={`selectPlayer${i + 1}${index}`}>{v}</DropdownItem>
  ))

  mapTeams = index => myTeams.map((v, i) => (
    <DropdownItem key={`teams${i + 1}${index}`} onClick={() => this.selectPlayer(`selectPlayer${i + 1}${index}`, index)} ref={`selectPlayer${i + 1}${index}`}>{v}</DropdownItem>
  ))

  handleChange = (e, index) => {
    console.log('testswrf');
    const prevState = this.state.playerInput;
    prevState[index] = e.target.value;
    this.setState({
      playerInput: [...prevState],
    }, () => {
      this.validateFix();
      console.log(this.state.playerInput);
    });
  }

  render() {
    return (
      <Container id="submit">
        <h1>Submit</h1>
        <Container id="submit__content">
          <Row>
            <Col md="5">
              <h2>Team 1</h2>
              {this.makeInputs(0, true, normaliseRadius)}
              {this.makeInputs(1, true, normaliseRadius)}
            </Col>
            <Col id="submit__h1" md="2">
              <h1 className="text-center">{this.state.mode}</h1>
            </Col>
            <Col md="5">
              <h2>Team 2</h2>
              {this.makeInputs(2, true, normaliseRadius)}
              {this.makeInputs(3, true, normaliseRadius)}
            </Col>
          </Row>
          <Row className="justify-content-center">
            <Col md="5" id="submit__dropdown__winners">
            <h2>Winners</h2>
            {this.makeInputs(4, false, normaliseRadius)}
            <p className="text-center" ref={'dab'}>{this.state.error}</p>
            </Col>
          </Row>
        </Container>
        <Row>
          <button className="submit__button" onClick={() => { this.getValues(safe, inputValues); }} >Submit</button>
        </Row>
        <div style={this.state.success ? displaySuccess : {}} className="submit__success__content">
          <Container>
            <Row>
              <Col md="6">
                <IconContext.Provider value={this.state.iconConfig}>
                  <FaCheckCircle/>
                </IconContext.Provider>
                <h3>Success!</h3>
                 <p>Sent scores to server</p>
              </Col>
              <Col className="hide" md="6">
                 <h3>Winner</h3>
                 <p>{this.props.previousSentForm.winner}</p>
              </Col>
            </Row>
            <Row>
              <Col md="6">
                  <h4 className="--top">Team 1:</h4>
                  <span className="span--top">
                    {this.displayProps('Team 1')}
                  </span>

              </Col>
              <Col md="6" className="hide">
                <h3 className="--positionFix">Date</h3>
                <p className="--positionFix">{this.props.previousSentForm.date}</p>
              </Col>
            </Row>
            <Row>
              <Col md="6">
                  <h4 className="--bottom">Team 2:</h4>
                  <span className="span--bottom">
                  {this.displayProps('Team 2')}
                  </span>

              </Col>
              <Col md="6" >

              </Col>
            </Row>
          </Container>
        </div>

      </Container>
    );
  }
}

const mapStateToProps = state => ({
  playersArrayProp: state.players.playersArrayProp,
  previousSentForm: state.form.previousSentForm,
});

const mapDispatchToProps = dispatch => bindActionCreators({ getPlayersActionCreator, sendFormObjectActionCreator }, dispatch);


export default connect(mapStateToProps, mapDispatchToProps)(Submit);
