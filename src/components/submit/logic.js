const logical = () => function () {
  // VS
  if (
    this.state.playerInput[0] !== 'Enter player name'
            && this.state.playerInput[1] === 'Enter player name'
            && this.state.playerInput[2] === 'Enter opponent name'
            && this.state.playerInput[3] === 'Enter opponent name'
  ) {
    this.setState({ mode: 'VS' });
  } else if (
    this.state.playerInput[0] === 'Enter player name'
        && this.state.playerInput[1] !== 'Enter player name'
        && this.state.playerInput[2] === 'Enter opponent name'
        && this.state.playerInput[3] === 'Enter opponent name'
  ) {
    this.setState({ mode: 'VS' });
  } else if (
    this.state.playerInput[0] === 'Enter player name'
        && this.state.playerInput[1] === 'Enter player name'
        && this.state.playerInput[2] !== 'Enter opponent name'
        && this.state.playerInput[3] === 'Enter opponent name'
  ) {
    this.setState({ mode: 'VS' });
  } else if (
    this.state.playerInput[0] === 'Enter player name'
    && this.state.playerInput[1] === 'Enter player name'
    && this.state.playerInput[2] === 'Enter opponent name'
    && this.state.playerInput[3] !== 'Enter opponent name'
  ) {
    this.setState({ mode: 'VS' });
  } else if (
    this.state.playerInput[0] !== 'Enter player name'
    && this.state.playerInput[1] !== 'Enter player name'
    && this.state.playerInput[2] === 'Enter opponent name'
    && this.state.playerInput[3] === 'Enter opponent name'
  ) {
    this.setState({ mode: 'VS' });
  } else if (
    this.state.playerInput[0] === 'Enter player name'
    && this.state.playerInput[1] === 'Enter player name'
    && this.state.playerInput[2] !== 'Enter opponent name'
    && this.state.playerInput[3] !== 'Enter opponent name'
  ) {
    this.setState({ mode: 'VS' });
  }

  // 1V1
  if (
    this.state.playerInput[0] !== 'Enter player name'
        && this.state.playerInput[1] === 'Enter player name'
        && this.state.playerInput[2] !== 'Enter opponent name'
        && this.state.playerInput[3] === 'Enter opponent name'
  ) {
    this.setState({ mode: '1v1' });
  } else if (
    this.state.playerInput[0] !== 'Enter player name'
        && this.state.playerInput[1] === 'Enter player name'
        && this.state.playerInput[2] === 'Enter opponent name'
        && this.state.playerInput[3] !== 'Enter opponent name'
  ) {
    this.setState({ mode: '1v1' });
  } else if (
    this.state.playerInput[1] !== 'Enter player name'
        && this.state.playerInput[0] === 'Enter player name'
        && this.state.playerInput[2] !== 'Enter opponent name'
        && this.state.playerInput[3] === 'Enter opponent name'
  ) {
    this.setState({ mode: '1v1' });
  } else if (
    this.state.playerInput[1] !== 'Enter player name'
        && this.state.playerInput[0] === 'Enter player name'
        && this.state.playerInput[2] === 'Enter opponent name'
        && this.state.playerInput[3] !== 'Enter opponent name'
  ) {
    this.setState({ mode: '1v1' });
  }

  // 1V2
  if (
    this.state.playerInput[0] !== 'Enter player name'
        && this.state.playerInput[1] === 'Enter player name'
        && this.state.playerInput[2] !== 'Enter opponent name'
        && this.state.playerInput[3] !== 'Enter opponent name'
  ) {
    this.setState({ mode: '1v2' });
  } else if (
    this.state.playerInput[1] !== 'Enter player name'
        && this.state.playerInput[0] === 'Enter player name'
        && this.state.playerInput[2] !== 'Enter opponent name'
        && this.state.playerInput[3] !== 'Enter opponent name'
  ) {
    this.setState({ mode: '1v2' });
  }

  // 2V1
  if (
    this.state.playerInput[0] !== 'Enter player name'
        && this.state.playerInput[1] !== 'Enter player name'
        && this.state.playerInput[2] !== 'Enter opponent name'
        && this.state.playerInput[3] === 'Enter opponent name'
  ) {
    this.setState({ mode: '2v1' });
  } else if (
    this.state.playerInput[0] !== 'Enter player name'
        && this.state.playerInput[1] !== 'Enter player name'
        && this.state.playerInput[2] === 'Enter opponent name'
        && this.state.playerInput[3] !== 'Enter opponent name'
  ) {
    this.setState({ mode: '2v1' });
  }

  // 2V2
  if (
    this.state.playerInput[0] !== 'Enter player name'
        && this.state.playerInput[1] !== 'Enter player name'
        && this.state.playerInput[2] !== 'Enter opponent name'
        && this.state.playerInput[3] !== 'Enter opponent name'
  ) {
    this.setState({ mode: '2v2' });
  }
};
export default logical;
