const getValues = () => function (a, b) {
    let safe = a
    let inputValues = b
    this.validateFix();
    if (safe) {
        // send form, clear inputs, and display/hide success prompt
        this.setState({
        success: true,
        });
        setTimeout(() => {
        this.setState({
            success: false,
        });
        }, 3000);
        const players = [];
        const winner = this.refs.playerInput4.props.value;
        const { mode } = this.state;
        const today = new Date();
        const date = `${today.getFullYear()}-${today.getMonth() + 1}-${today.getDate()}`;
        console.log(date);

        if (this.refs.playerInput0.props.value !== 'Enter player name') {
        players.push([this.refs.playerInput0.props.value, 'Team 1']);
        }
        if (this.refs.playerInput1.props.value !== 'Enter player name') {
        players.push([this.refs.playerInput1.props.value, 'Team 1']);
        }

        if (this.refs.playerInput2.props.value !== 'Enter opponent name') {
        players.push([this.refs.playerInput2.props.value, 'Team 2']);
        }
        if (this.refs.playerInput3.props.value !== 'Enter opponent name') {
        players.push([this.refs.playerInput3.props.value, 'Team 2']);
        }
        inputValues = {
        players,
        winner,
        mode,
        date,
        };
        this.props.sendFormObjectActionCreator(inputValues);
        console.log('Heres your redux prop ');
        console.log(this.props.previousSentForm);
        this.setState({
        mode: 'VS',
        });
        this.setState({
        playerInput: [...['Enter player name', 'Enter player name', 'Enter opponent name', 'Enter opponent name', 'Select winning team']],
        }, () => {
        this.validateFix();
        });
        console.log('safe');
    } else {
        console.log('unsafe');
        // dont send form
    }  
}

export default getValues;