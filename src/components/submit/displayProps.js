const displayProps = () => function (team) {
    const team1 = [];
    const team2 = [];
    if (typeof this.props.previousSentForm.players !== 'undefined') {
      this.props.previousSentForm.players.forEach((v, i) => {
        if (v[1] == 'Team 1') {
          team1.push(v[0]);
        } else {
          team2.push(v[0]);
        }
      });
    }

    if (team1.length > 1) {
      team1.splice(1, 0, ', ');
    }
    if (team2.length > 1) {
      team2.splice(1, 0, ', ');
    }

    if (team == 'Team 1') {
      return team1;
    }
    return team2;
}
export default displayProps