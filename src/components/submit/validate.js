const validate = () => function () {
  const valuesObject = {};
  valuesObject[0] = this.refs.playerInput0.props.value;
  valuesObject[1] = this.refs.playerInput1.props.value;
  valuesObject[2] = this.refs.playerInput2.props.value;
  valuesObject[3] = this.refs.playerInput3.props.value;
  valuesObject[4] = this.refs.playerInput4.props.value;


  const winner = valuesObject[4] = this.refs.playerInput4.props.value;
  const default1 = 'Enter player name';
  const default2 = 'Enter opponent name';
  let leftCheck = false;
  let rightCheck = false;


  // console.log('clicked')

  for (let i = 0; i < 4; i++) {
    // check left side
    if (i == 0 || i == 1) {
      // console.log(i)
      // console.log('checking left side')
      if (valuesObject[i] !== default1) {
        leftCheck = true;
      }
    }

    // check right side
    if (i == 2 || i == 3) {
      if (valuesObject[i] !== default2) {
        rightCheck = true;
      }
    }
  }

  if (leftCheck) {
    // console.log('Found unique left player')
  } else if (rightCheck) {
    // console.log('Found unique right player')
  }

  if (leftCheck && rightCheck) {
    // console.log('left and right is true')
    this.setState({
      error: '',
    });
  } else {
    // console.log('Need at least one player a team')
    this.setState({
      error: 'Need at least one player a team',
    });
  }

  for (const property in valuesObject) {
    if (valuesObject[property] == default1) {
      // console.log(valuesObject[property] + ' is equal to ' + default1)
    } else if (valuesObject[property] == default2) {
      // console.log(valuesObject[property] + ' is equal to ' + default2)
    } else {
      // console.log('\n')
      // console.log(valuesObject[property] + ' is not equal to any default')

      for (const property2 in valuesObject) {
        if (property !== property2) {
          // console.log('Looping through all properties other than myself and winnings')

          if (valuesObject[property] == valuesObject[property2]) {
            // console.log('Player isnt unique ' + valuesObject[property] + " is the same as " + valuesObject[property2])
            this.setState({
              error: 'All players must be unique',
            });
          }
        }
      }
    }
  }


  for (const property in valuesObject) {
    if (valuesObject[property].length < 1) {
      this.setState({
        error: "Fields can't be blank. Leave unused fields as default.",
      });
    } else if (!valuesObject[property].match(/^[-_ a-zA-Z0-9]+$/)) {
      // console.log('Contains non alphanumeric property')
      this.setState({
        error: 'Fields must contain alphanumeric characters.',
      });
    } else if (winner == 'Select winning team') {
      this.setState({
        error: 'Winner cannot be blank',
      });
    }
  }
};
export default validate;
