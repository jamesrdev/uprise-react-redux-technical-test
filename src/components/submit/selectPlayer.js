const selectPlayer = () => function (r, index) {
    let selectedPlayer = this.refs[r].props.children;
    const prevState = this.state.playerInput;
    if (selectedPlayer === 'Clear Selection') {
        switch (index) {
        case 0:
            prevState[index] = 'Enter player name';
            break;
        case 1:
            prevState[index] = 'Enter player name';
            break;
        case 2:
            prevState[index] = 'Enter opponent name';
            break;
        case 3:
            prevState[index] = 'Enter opponent name';
            break;
        }
    } else {
        prevState[index] = selectedPlayer;
    }

    this.setState({
        playerInput: [...prevState],
    }, () => {
        this.logicalFix();
        this.validateFix();
    });
}

export default selectPlayer;