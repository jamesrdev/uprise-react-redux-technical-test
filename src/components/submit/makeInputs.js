import React from 'react';
import {
    InputGroup,
    InputGroupButtonDropdown,
    Input,
    DropdownToggle,
    DropdownMenu,
  } from 'reactstrap';

const makeInputs = () => function (index, tern = true, rady) { 
    let normaliseRadius = rady
    return (
    <InputGroup >
    <Input disabled={!tern} ref={`playerInput${index}`} style={this.state.dropdownOpen[index] ? normaliseRadius : null } onChange={e => this.handleChange(e, index)} value={this.state.playerInput[index]}/>
    <InputGroupButtonDropdown addonType="append" isOpen={this.state.dropdownOpen[index]} toggle={() => { this.toggleDropDown(index); }} >
      <DropdownToggle style={this.state.dropdownOpen[index] ? normaliseRadius : null } caret>
      </DropdownToggle>
      <DropdownMenu>
       {
         tern ? this.mapPlayers(index) : this.mapTeams(index)
        }
      </DropdownMenu>
    </InputGroupButtonDropdown>
  </InputGroup>
    )
}
export default makeInputs