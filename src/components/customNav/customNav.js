import React, { Component } from 'react';
import './customNav.scss';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
} from 'reactstrap';
import { NavLink as RRNavLink } from 'react-router-dom';
import FoosballIcon from '../../assets/foosballTrans.png';

class CustomNav extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false,
    };
  }

  componentWillMount() {
    if (this.props.test == true) {
      window.location.pathname = '/';
      console.log('running this code');
    }
  }

  toggle() {
    this.setState({
      isOpen: !this.state.isOpen,
    });
  }

  render() {
    return (
      <Navbar id="customNav" light expand="md">
        <NavbarBrand tag={RRNavLink} to="/">
         {/*  <img src={FoosballIcon}></img> */}
        </NavbarBrand>
        <NavbarToggler color="white" onClick={this.toggle} />

        <Collapse
          className="justify-content-center"
          isOpen={this.state.isOpen}
          navbar
        >
          <Nav navbar>
            <NavItem>
              <NavLink
                tag={RRNavLink}
                to="/"
                activeClassName="customNav__navLink--active"
                exact
                path="/"
              >
              <img src={FoosballIcon}></img><img src={FoosballIcon}></img>
                Leaderboard
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                tag={RRNavLink}
                to="/submit"
                activeClassName="customNav__navLink--active"
                exact
                path="/submit"
              >
                Submit
              </NavLink>
            </NavItem>
          </Nav>
        </Collapse>
      </Navbar>
    );
  }
}

export default CustomNav;
