import React from 'react';

const handletvo = () => function () {
    if (
      this.props.uniquePlayerArrayProp !== 'undefined'
          && typeof this.props.uniquePlayerArrayProp[0] !== 'undefined'
    ) {
      const x = [];

      const rank = this.props.uniquePlayerArrayProp.map((v, i) => {
        for (const property in v) {
          const obj = v[property]['2v1'];
          for (const property2 in obj) {
            const q = [];
            const obj2 = obj[property2];
            for (const property3 in obj2) {
              const f = [];
              obj2[property3].games.forEach((v, i) => {
                f.push(
                                  <div>
                                      <p className="player__game --game2">Game {i + 1}</p>
                                      <p className="player__item --item2"><span className="darken">Date:</span> {v.date}</p>
                                      <p className="player__item --item2"><span className="darken">Result:</span> {v.win == true ? 'win' : 'loss'}</p>
                                  </div>,
                );
              });
              q.push(
                          <div>
                              <p className="player__opponent">{property3}</p>
                              {f}
                          </div>,

              );
            }

            x.push(
                          <div key={Math.floor(Math.random() * 1000 + i)}>
                              <p className="player__name" key={Math.floor(Math.random() * 1000 + i)}>{property2}</p>
                              {
                                  q
                              }
                          </div>,
            );
          }
        }
        return x;
      });
      return rank;
   
  }
}
export default handletvo;