import React from 'react';

const handleovt = () => function () {
    const ovo = {};
    let rank = [];

    if (
        this.props.uniquePlayerArrayProp !== 'undefined'
            && typeof this.props.uniquePlayerArrayProp[0] !== 'undefined'
    ) {
        for (const property in this.props.uniquePlayerArrayProp[0]) {
        const obj = this.props.uniquePlayerArrayProp[0][property]['1v2'];
        for (const property2 in obj) {
            ovo[property2] = {
            name: property2,
            games: [],
            };
            obj[property2].games.forEach((v, i) => {
            // console.log(v)
            ovo[property2].games.push(v);
            });
        }
        }

        for (const property in ovo) {
        rank.push({
            name: ovo[property].name,
            games: ovo[property].games,
        });
        }

        rank = rank.map((v, i) => {
        let games = [];
        games = v.games.map((vv, ii) => (
                        <div key={Math.floor(Math.random() * 1000 + i) + 1} >
                            <p className="player__game" key={Math.floor(Math.random() * 1000 + i)}>Game {ii + 1}</p>
                            <p className="player__item" key={Math.floor(Math.random() * 1000 + i)}><span className="darken">Date:</span> {vv.date}</p>
                            <p className="player__item" key={Math.floor(Math.random() * 1000 + i)}><span className="darken">Result:</span> {vv.win == true ? 'win' : 'loss'}</p>
                        </div>
        ));

        return (
                    <div key={Math.floor(Math.random() * 100 + i)}>
                        <p className="player__name" key={Math.floor(Math.random() * 1000 + i)}>{v.name}</p>
                        <div key={Math.floor(Math.random() * 1000 + i)}>
                            {games}
                        </div>
                    </div>
        );
        });
        return rank;
    }
}
export default handleovt;
