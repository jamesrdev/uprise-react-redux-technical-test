import React, { Component } from 'react';
import { Container, Row, Col } from 'reactstrap';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { getOnePlayerActionCreator } from '../../redux/actions/actions';
import './player.scss';
import handleovo from './handleovo';
import handletvo from './handletvo';
import handleovt from './handleovt';
import handletvt from './handletvt'
var LineChart = require("react-chartjs").Line;
let chartData
let style = {
  position: 'relative',
  height: '200px',
  width: '500px'
}


let options = {
  responsive: true,
  maintainAspectRatio: false,
  tooltipTemplate: "<%= label %>: <%= value %>%",
  scales: {
    yAxes: [{
        ticks: {
            // Include a dollar sign in the ticks
            callback: function(value, index, values) {
                return value + "%";
            }
        }
    }]
}
}



class Player extends Component {
  componentWillMount() {
    const url = window.location.href;
    const procUrl = url.split('/');
    const index = procUrl.length - 1;
    const page = `/${procUrl[index]}`;
    console.log(page);
    this.props.getOnePlayerActionCreator(page);
    chartData = this.handleWinsTime()
  }

    handle1v1 = handleovo()
    handle2v1 = handletvo()
    handle1v2 = handleovt()
    handle2v2 = handletvt()
    handleWinsTime = () => {
      if (
        this.props.uniquePlayerArrayProp !== 'undefined'
            && typeof this.props.uniquePlayerArrayProp[0] !== 'undefined'
    ) {
        let gamesArray = []
        console.log('called your wins over time')
        let obj = this.props.uniquePlayerArrayProp[0]
        for (const property in obj) {
          let obj2 = obj[property]
            for (let property2 in obj2) {
              let obj3 = obj2[property2]
              for (let property3 in obj3) {
                if (typeof obj3[property3].games !== 'undefined') {
                  obj3[property3].games.forEach((v,i) => {
                    gamesArray.push(v)
                  })
                } else {
                  let obj4 = obj3[property3]
                  for (let property4 in obj4) {
                    if (typeof obj4[property4].games !== 'undefined') {
                      obj4[property4].games.forEach((vv, i) => {
                        gamesArray.push(vv)
                      })
                    }
                  }
                }
              }
            }
        }
        gamesArray = gamesArray.sort(function(a, b) {
          a = new Date(a.date);
          b = new Date(b.date);
          return a>b ? 1 : a<b ? -1 : 0;
      });
      let winsOverTime = {}
      let wins = 0
       gamesArray.forEach((v, i) => {
         if (v.win == true) {
          wins++
         }
         winsOverTime[v.date] = {
           date: v.date,
           ratio: Math.trunc((wins / (i+1)) * 100)
         }
       })
       let winsSorted = []
       for (let property in winsOverTime) {
         winsSorted.push(winsOverTime[property])
       }
       let labels = []
       let data = []
       labels = winsSorted.map((v,i) => {
        return v.date
       })
       data = winsSorted.map((v,i) => {
         return v.ratio
       })

       let myData = {
          labels: labels,
          datasets: [
            {
              label: "My First dataset",
              fillColor: "rgba(220,220,220,0.2)",
              strokeColor: "rgba(220,220,220,1)",
              pointColor: "rgba(220,220,220,1)",
              pointStrokeColor: "#fff",
              pointHighlightFill: "#fff",
              pointHighlightStroke: "rgba(220,220,220,1)",
              data:data
            },
          ]
        };
        console.log(myData)
        return myData
      }
    }
    render() {
      {chartData = this.handleWinsTime()}
      return (
            <Container id="player">
                <h1>
                    {
                        typeof this.props.uniquePlayerArrayProp !== 'undefined'
                        && typeof this.props.uniquePlayerArrayProp[0] !== 'undefined'
                          ? this.props.uniquePlayerArrayProp[0].name : ''
                    }
                </h1>
                <Container id="player__content">
                    <Row>
                        <div className="player__content__col1">
                            <h2>1v1</h2>
                            {this.handle1v1()}
                        </div>
                        <div className="player__content__col2">
                            <h2>2v1</h2>
                            {this.handle2v1()}
                        </div>
                        <div className="player__content__col3">
                            <h2>1v2</h2>
                            {this.handle1v2()}
                        </div>
                        <div className="player__content__col4">
                            <h2>2v2</h2>
                            {this.handle2v2()}
                        </div>
                    </Row>
                    <div>
                    <h2>Wins : Time</h2>
                    <Row> 
                      <Col md="10" sm="12" xs="12">
                    {
                      typeof chartData !== 'undefined' ? <LineChart data={chartData} options={options} /> : ''

                    }
                    </Col>
                    </Row>
                    </div>
                </Container>
            </Container>
      );
    }
}

const mapStateToProps = state => ({
  uniquePlayerArrayProp: state.unique.uniquePlayerArrayProp,
});

const mapDispatchToProps = dispatch => bindActionCreators({ getOnePlayerActionCreator }, dispatch);


export default connect(mapStateToProps, mapDispatchToProps)(Player);
