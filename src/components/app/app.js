import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import './app.scss';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Navbar from '../customNav/customNav';
import Leaderboard from '../leaderboard/leaderboard';
import Submit from '../submit/submit';
import Player from '../player/player';
import { getLinksActionCreator } from '../../redux/actions/actions';

class App extends Component {
  componentWillMount() {
    this.props.getLinksActionCreator();
  }

  condRenderPlayerRoutes() {
    if (typeof this.props.linksArrayProp !== 'undefined' && typeof this.props.linksArrayProp[0] !== 'undefined') {
      return (
        <Route path={this.props.linksArrayProp} render={props => (<Player {...props} />)} />
      );
    }
    return (
        <div></div>
    );
  }

  render() {
    return (
      <Router>
          <Navbar />
            <Route exact path="/" component={Leaderboard} />
            <Route exact path="/submit" component={Submit} />
            {this.condRenderPlayerRoutes()}
      </Router>
    );
  }
}


const mapStateToProps = state => ({
  linksArrayProp: state.links.linksArrayProp,
});

const mapDispatchToProps = dispatch => bindActionCreators({ getLinksActionCreator }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(App);
