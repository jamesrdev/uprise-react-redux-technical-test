import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import './leaderboard.scss';
import { Container, Row} from 'reactstrap';
import { getRankingsActionCreator, getLinksActionCreator } from '../../redux/actions/actions';
import  displayRankings from './displayRankings'
let rankings

class Leaderboard extends Component {
  constructor(props) {
    super(props)
  }

  componentWillMount() {
    this.props.getRankingsActionCreator();
    this.props.getLinksActionCreator();
  }


  displayRankings = displayRankings()

  render() {
    rankings = this.displayRankings()
    return (
      <Container id="leaderboard">
        <h1>Leaderboard</h1>
        <Container id="leaderboard__content">
          <Row>
            <div className="leaderboard__column1">
            <h2>Rank</h2>
             {
               typeof this.props.rankingsArrayProp !== 'undefined' && typeof rankings !== 'undefined' ?
               rankings.col1 : ''
               }
            </div>
            <div className="leaderboard__column2">
              <h2>1v1</h2>
              {
                typeof this.props.rankingsArrayProp !== 'undefined'  && typeof rankings !== 'undefined'  ?
                rankings.col2 : ''
              }
            </div>
            <div className="leaderboard__column3">
              <h2>2v1</h2>
              {
                typeof this.props.rankingsArrayProp !== 'undefined'  && typeof rankings !== 'undefined'  ?
                rankings.col3 : ''
                }
            </div>
            <div className="leaderboard__column4">
              <h2>1v2</h2>
              {
                typeof this.props.rankingsArrayProp !== 'undefined'  && typeof rankings !== 'undefined'  ?
                rankings.col4 : ''
                }
            </div>
            <div className="leaderboard__column5">
              <h2>2v2</h2>
              {
                typeof this.props.rankingsArrayProp !== 'undefined'  && typeof rankings !== 'undefined'  ?
                rankings.col5 : ''
                }
            </div>
            <div className="leaderboard__column6">
              <h2>Overall</h2>
              {
                typeof this.props.rankingsArrayProp !== 'undefined'  && typeof rankings !== 'undefined' ?
                rankings.col6 : ''
                }
            </div>
          </Row>
          
       </Container>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  rankingsArrayProp: state.rankings.rankingsArrayProp,
});

const mapDispatchToProps = dispatch => bindActionCreators({ getRankingsActionCreator, getLinksActionCreator }, dispatch);


export default connect(mapStateToProps, mapDispatchToProps)(Leaderboard);
