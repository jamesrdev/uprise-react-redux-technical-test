import React from 'react';
import { Row } from 'reactstrap';
import { Link } from 'react-router-dom';

const displayRankings = () => function () {
    // get 1v1 rankings
    const oneVoneGetter = () => {
        console.log('called function');
        const oneVoneRank = {};

        // console.log('data accessible')
        this.props.rankingsArrayProp.forEach((v, i) => {
        oneVoneRank[v.name] = {
            name: v.name,
            games: [],
            total: 0,
            wins: 0,
        };
        const obj = v[v.name]['1v1'];
        // console.log(v.name)
        // console.log(v[v.name]["1v1"])
        for (const property in obj) {
            // console.log('alerting games')
            oneVoneRank[v.name].games.push(obj[property].games);
        }
        // console.log('-----------------------')
        });


        for (const property in oneVoneRank) {
        oneVoneRank[property].games = oneVoneRank[property].games.flat();
        const { length } = oneVoneRank[property].games;
        for (let i = 0; i < length; i++) {
            if (oneVoneRank[property].games[i].win == true) {
            oneVoneRank[property].wins++;
            oneVoneRank[property].total++;
            } else {
            oneVoneRank[property].total++;
            }
        }
        }

        const rank = [];
        for (const property in oneVoneRank) {
        if (oneVoneRank[property].total > 0) {
            const temp = {
            name: oneVoneRank[property].name,
            total: oneVoneRank[property].total,
            wins: oneVoneRank[property].wins,
            ratio: Math.trunc((oneVoneRank[property].wins / oneVoneRank[property].total) * 100),
            };
            rank.push(temp);
        }
        }

        rank.sort((a, b) => ((a.ratio < b.ratio) ? 1 : (a.ratio === b.ratio) ? ((a.name > b.name) ? 1 : -1) : -1));
        return rank;
    };


    // get 2v1 rankings
    const twoVoneGetter = () => {
        const twoVoneKeys = [];
        let removeDupKeys = [];
        this.props.rankingsArrayProp.forEach((v, i) => {
        const obj = v[v.name]['2v1'];
        twoVoneKeys.push(obj);
        });


        twoVoneKeys.forEach((v, i) => {
        // console.log('alerting the stored 2v1s in array')
        removeDupKeys.push(Object.keys(v));
        });
        removeDupKeys = removeDupKeys.flat();
        // console.log(removeDupKeys)

        const sortingObj = {};

        removeDupKeys.forEach((v, i) => {
        let splitPair = v.split('+');

        splitPair = splitPair.map((v, i) => {
            if (i == 0) {
            const subStr = v.substring(0, v.length - 1);
            return (
                subStr
            );
            }
            const subStr = v.substr(1);
            return (
            subStr
            );
        });
        splitPair = splitPair.sort();
        sortingObj[`${splitPair[0]} + ${splitPair[1]}`] = {
            raw: splitPair,
            processed: `${splitPair[0]} + ${splitPair[1]}`,
        };
        });

        const twoVoneSorted = {};
        const sortingObjKeys = Object.keys(sortingObj);
        this.props.rankingsArrayProp.forEach((v, i) => {
        sortingObjKeys.forEach((key, i) => {
            twoVoneSorted[key] = {
            name: key,
            total: 0,
            wins: 0,
            };
        });
        });

        this.props.rankingsArrayProp.forEach((v, i) => {
        sortingObjKeys.forEach((key, i) => {
            const obj = v[v.name]['2v1'][key];

            for (const property in obj) {
            obj[property].games.forEach((vv, i) => {
                // console.log(vv)
                if (vv.win == true) {
                // console.log('true')
                // console.log(twoVoneSorted[key].wins)
                twoVoneSorted[key].wins++;
                twoVoneSorted[key].total++;
                } else {
                twoVoneSorted[key].total++;
                }
            });
            }
        });
        });

        const rank = [];
        for (const property in twoVoneSorted) {
        if (twoVoneSorted[property].total > 0) {
            const temp = {
            name: twoVoneSorted[property].name,
            total: twoVoneSorted[property].total,
            wins: twoVoneSorted[property].wins,
            ratio: Math.trunc((twoVoneSorted[property].wins / twoVoneSorted[property].total) * 100),
            };
            rank.push(temp);
        }
        }

        rank.sort((a, b) => ((a.ratio < b.ratio) ? 1 : (a.ratio === b.ratio) ? ((a.name > b.name) ? 1 : -1) : -1));
        console.log(rank);
        return rank;
    };


    // Get 1v2 rankins
    const oneVtwoGetter = () => {
        const oneVtwoOrdered = {};
        this.props.rankingsArrayProp.forEach((v, i) => {
        oneVtwoOrdered[v.name] = {
            name: v.name,
            total: 0,
            wins: 0,
        };
        });

        this.props.rankingsArrayProp.forEach((v, i) => {
        const obj = v[v.name]['1v2'];
        // console.log(obj)
        for (const property in obj) {
            obj[property].games.forEach((vv, i) => {
            // console.log(v)
            if (vv.win == true) {
                oneVtwoOrdered[v.name].wins++;
                oneVtwoOrdered[v.name].total++;
            } else {
                oneVtwoOrdered[v.name].total++;
            }
            });
        }
        });

        const rank = [];
        for (const property in oneVtwoOrdered) {
        if (oneVtwoOrdered[property].total > 0) {
            const temp = {
            name: oneVtwoOrdered[property].name,
            total: oneVtwoOrdered[property].total,
            wins: oneVtwoOrdered[property].wins,
            ratio: Math.trunc((oneVtwoOrdered[property].wins / oneVtwoOrdered[property].total) * 100),
            };
            rank.push(temp);
        }
        }

        rank.sort((a, b) => ((a.ratio < b.ratio) ? 1 : (a.ratio === b.ratio) ? ((a.name > b.name) ? 1 : -1) : -1));
        return rank;
    };

    // Get 2v2 rankings
    const twoVtwoGetter = () => {
        const unsortedKeys = [];
        this.props.rankingsArrayProp.forEach((v, i) => {
        const obj = v[v.name]['2v2'];
        unsortedKeys.push(Object.keys(obj));
        });

        let sortObj = {};
        unsortedKeys.forEach((v, i) => {
        if (typeof v[0] !== 'undefined') {
            let split = v[0].split(' + ');
            split = split.sort();
            split.splice(1, 0, ' + ');
            split = split.join('');
            sortObj[`${split}`] = `${split}`;
        }
        });
        sortObj = Object.keys(sortObj);
        const twovtwosorted = {};
        sortObj.forEach((vv, i) => {
        this.props.rankingsArrayProp.forEach((v, i) => {
            const obj = v[v.name]['2v2'];
            twovtwosorted[vv] = {
            name: vv,
            total: 0,
            wins: 0,
            };
        });
        });


        sortObj.forEach((vv, i) => {
        this.props.rankingsArrayProp.forEach((v, i) => {
            const obj = v[v.name]['2v2'];
            for (const property in obj[vv]) {
            obj[vv][property].games.forEach((vvv, i) => {
                if (vvv.win == true) {
                twovtwosorted[vv].wins++;
                twovtwosorted[vv].total++;
                } else {
                twovtwosorted[vv].total++;
                }
            });
            }
        });
        });

        const rank = [];
        for (const property in twovtwosorted) {
        if (twovtwosorted[property].total > 0) {
            const temp = {
            name: twovtwosorted[property].name,
            total: twovtwosorted[property].total,
            wins: twovtwosorted[property].wins,
            ratio: Math.trunc((twovtwosorted[property].wins / twovtwosorted[property].total) * 100),
            };
            rank.push(temp);
        }
        }

        rank.sort((a, b) => ((a.ratio < b.ratio) ? 1 : (a.ratio === b.ratio) ? ((a.name > b.name) ? 1 : -1) : -1));
        return rank;
    };

    const overallscoresGetter = () => {
        const overAll = {};
        this.props.rankingsArrayProp.forEach((v, i) => {
        overAll[v.name] = {
            name: v.name,
            total: 0,
            wins: 0,
        };
        });

        this.props.rankingsArrayProp.forEach((v, i) => {
        const obj = v[v.name];
        for (const property in obj) {
            const obj2 = obj[property];
            for (const property2 in obj2) {
            if (typeof obj2[property2].games !== 'undefined') {
                obj2[property2].games.forEach((vv, i) => {
                if (vv.win == true) {
                    overAll[v.name].wins++;
                    overAll[v.name].total++;
                } else {
                    overAll[v.name].total++;
                }
                });
            } else {
                const obj3 = obj2[property2];
                for (const property3 in obj3) {
                obj3[property3].games.forEach((vvv, i) => {
                    if (vvv.win == true) {
                    overAll[v.name].wins++;
                    overAll[v.name].total++;
                    } else {
                    overAll[v.name].total++;
                    }
                });
                }
            }
            }
        }
        });


        const rank = [];
        for (const property in overAll) {
        if (overAll[property].total > 0) {
            const temp = {
            name: overAll[property].name,
            total: overAll[property].total,
            wins: overAll[property].wins,
            ratio: Math.trunc((overAll[property].wins / overAll[property].total) * 100),
            };
            rank.push(temp);
        }
        }

        rank.sort((a, b) => ((a.ratio < b.ratio) ? 1 : (a.ratio === b.ratio) ? ((a.name > b.name) ? 1 : -1) : -1));
        return rank;
    };

    if (this.props.rankingsArrayProp !== 'undefined') {
        // 1 V 1
        const onevonescores = oneVoneGetter();

        // 2 V 1
        const twovonecores = twoVoneGetter();

        // 1 V 2
        const onevtwoscores = oneVtwoGetter();

        // 2 V 2
        const twovtwoscores = twoVtwoGetter();

        // Overall
        const overallscores = overallscoresGetter();

        const maxLengthArray = [];
        maxLengthArray.push(onevonescores.length);
        maxLengthArray.push(twovonecores.length);
        maxLengthArray.push(onevtwoscores.length);
        maxLengthArray.push(twovtwoscores.length);
        maxLengthArray.push(overallscores.length);

        const maxLength = Math.max(...maxLengthArray);
        let masterObject = {}
        let col1Array = []
        let col2Array = []
        let col3Array = []
        let col4Array = []
        let col5Array = []
        let col6Array = []


        for (let i = 0; i < maxLength; i++) {
        let oVoLink;
        if (typeof onevonescores[i] !== 'undefined' && typeof onevonescores[i].name !== 'undefined') {
            oVoLink = onevonescores[i].name.replace(/\s+/g, '-').toLowerCase();
            oVoLink = `/${oVoLink}`;
        }

        let tVoLink;
        let tVoNames;
        if (typeof twovonecores[i] !== 'undefined' && typeof twovonecores[i].name !== 'undefined') {
            tVoNames = twovonecores[i].name.split(' + ');
            tVoLink = twovonecores[i].name.split(' + ');
            tVoLink[0] = tVoLink[0].replace(/\s+/g, '-').toLowerCase();
            tVoLink[0] = `/${tVoLink[0]}`;

            tVoLink[1] = tVoLink[1].replace(/\s+/g, '-').toLowerCase();
            tVoLink[1] = `/${tVoLink[1]}`;
        }

        let oVtLink;
        if (typeof onevtwoscores[i] !== 'undefined' && typeof onevtwoscores[i].name !== 'undefined') {
            oVtLink = onevtwoscores[i].name.replace(/\s+/g, '-').toLowerCase();
            oVtLink = `/${oVtLink}`;
        }

        let tvtLink;
        let tvtName;
        if (typeof twovtwoscores[i] !== 'undefined' && typeof twovtwoscores[i].name !== 'undefined') {
            tvtName = twovtwoscores[i].name.split(' + ');
            tvtLink = twovtwoscores[i].name.split(' + ');
            tvtLink[0] = tvtLink[0].replace(/\s+/g, '-').toLowerCase();
            tvtLink[0] = `/${tvtLink[0]}`;

            tvtLink[1] = tvtLink[1].replace(/\s+/g, '-').toLowerCase();
            tvtLink[1] = `/${tvtLink[1]}`;
        }

        let overallLink;
        if (typeof overallscores[i] !== 'undefined' && typeof overallscores[i].name !== 'undefined') {
            overallLink = overallscores[i].name.replace(/\s+/g, '-').toLowerCase();
            overallLink = `/${overallLink}`;
        }


        col1Array.push(
            <React.Fragment>
                <p className="font-weight-bold">{`${i + 1}`}</p>
            </React.Fragment>
            )
        masterObject["col1"] = col1Array

        col2Array.push(
            <React.Fragment>
                {
                    typeof onevonescores[i] !== 'undefined' && typeof onevonescores[i].name !== 'undefined' && typeof oVoLink !== 'undefined'
                    ? <p>
                    <Link to={oVoLink}><span className="leaderboard__link">{onevonescores[i].name}</span></Link>: {onevonescores[i].ratio}% wins
                    </p>
                    : ''
                }
            </React.Fragment>
        )
        masterObject["col2"] = col2Array

        col3Array.push(
            <React.Fragment>
                {
                typeof twovonecores[i] !== 'undefined' && typeof twovonecores[i].name !== 'undefined' && typeof tVoLink !== 'undefined'
                    ? <p>
                    <Link to={tVoLink[0]}><span className="leaderboard__link">{tVoNames[0]}</span></Link>
                        <span> + </span><Link to={tVoLink[1]}><span className="leaderboard__link">{tVoNames[1]}</span>
                    </Link> : {twovonecores[i].ratio}% wins
                    </p>
                    : ''
                }
            </React.Fragment>
        )
        masterObject["col3"] = col3Array

        col4Array.push(
            <React.Fragment>
                {
                    typeof onevtwoscores[i] !== 'undefined' && typeof onevtwoscores[i].name !== 'undefined'
                    ? <p>
                        <Link to={oVtLink}><span className="leaderboard__link">{onevtwoscores[i].name}</span></Link>: {onevtwoscores[i].ratio}% wins
                    </p>
                    : ''
                }
            </React.Fragment>
        )
        masterObject["col4"] = col4Array

        col5Array.push(
            <React.Fragment>
                {
                    typeof twovtwoscores[i] !== 'undefined' && typeof twovtwoscores[i].name !== 'undefined'
                    ? <p>
                        <Link to={tvtLink[0]}><span className="leaderboard__link">{tvtName[0]}</span></Link>
                        <span> + </span><Link to={tvtLink[1]}><span className="leaderboard__link">{tvtName[1]}</span>
                        </Link> : {twovtwoscores[i].ratio}% wins
                    </p>
                    : ''
                }
            </React.Fragment>
            )
            masterObject["col5"] = col5Array

            col6Array.push(
                <React.Fragment>
                {
                    typeof overallscores[i] !== 'undefined' && typeof overallscores[i].name !== 'undefined'
                    ? <p>
                        <Link to={overallLink}><span className="leaderboard__link">{overallscores[i].name}</span></Link>: {overallscores[i].ratio}% wins
                    </p>
                    : ''
                }
                </React.Fragment>
            )
            masterObject["col6"] = col6Array
        }
        return masterObject
    }
}

export default displayRankings