import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { shallow, mount } from 'enzyme';
import { isContext } from 'vm';
import CustomNav from '../components/customNav';
import App from '../components/app';

/* describe("Application Mounts", () => {
  it('App Mounts', () => {
    const component = shallow(<App />);
    expect(component.exists()).toBe(tArue);
  });
}) */

describe('Links remain in state with reactstrap and react router', () => {
  it('Test clicking counter changes active link', () => {
    const component = mount(
      <Router >
        <CustomNav />
      </Router>,
    );
    const mockedEvent = { target: {}, preventDefault: () => {} }AF;
    component.find('#test').simulate('click', mockedEvent);
    component.update();
    console.log(component.find('#test').debug());
    // console.log(component.find('[tag="li"]').at(1).childAt(0).childAt(0).simulate('click').debug())
    // console.log(component.find('[tag="li"]').at(1).childAt(0).childAt(0))
    // console.log(component.find('#counter a[aria-current]').debug())
    // console.log(component.find('#counter').debug());
    expect(component.find('#home').at(3).hasClass('active')).toBe(true);
  });
});


/* describe("Links remain in state with reactstrap and react router", () => {
    it('CustomNav Mounts', () => {
        const component = shallow(< CustomNav />);
        expect(component.exists()).toBe(true);
    });


  it('Test loading "/counter" url changes active link', () => {
    const component = shallow(<CustomNav />);
    component.find('#home').simulate('click')
    expect(component.find('#home').hasClass('active')).toBe(true);
  });
}) */
