export const getPlayersActionCreator = payload => (dispatch) => {
  fetch('/playerList').then((response) => {
    response.text().then((text) => {
      dispatch({
        type: 'GET_PLAYERS',
        payload: JSON.parse(text),
      });
    });
  });
};

export const getRankingsActionCreator = payload => (dispatch) => {
  fetch('/rankings').then((response) => {
    response.text().then((text) => {
      dispatch({
        type: 'GET_RANKINGS',
        payload: JSON.parse(text),
      });
    });
  });
};

export const sendFormObjectActionCreator = payload => (dispatch) => {
  fetch('/sendForm', {
    method: 'POST',
    body: JSON.stringify(payload),
    headers: {
      'Content-Type': 'application/json',
    },
  })
    .then((response) => {
      response.text().then((text) => {
        console.log('made it here');
        console.log(JSON.parse(text));

        // Server finished adding form to database, so get the new list of players for our dropdowns
        dispatch(getPlayersActionCreator());
        dispatch(getRankingsActionCreator());
      });
    });
  dispatch({
    type: 'SEND_FORM',
    payload,
  });
};

export const getLinksActionCreator = payload => (dispatch) => {
  fetch('/links').then((response) => {
    response.text().then((text) => {
      dispatch({
        type: 'GET_LINKS',
        payload: JSON.parse(text),
      });
    });
  });
};

export const getOnePlayerActionCreator = payload => (dispatch) => {
  fetch('/player', {
    method: 'POST',
    body: JSON.stringify({ link: payload }),
    headers: {
      'Content-Type': 'application/json',
    },
  })
    .then((response) => {
      response.text().then((text) => {
        dispatch({
          type: 'GET_ONE_PLAYER',
          payload: JSON.parse(text),
        });
      });
    });
};
