import { combineReducers } from 'redux';
import { getPlayersReducer } from './getPlayersReducer';
import { sendFormReducer } from './sendFormReducer';
import { getRankingsReducer } from './getRankingsReducer';
import { getLinksReducer } from './getLinksReducer';
import { getOnePlayerReducer } from './getOnePlayerReducer';

export default combineReducers({
  players: getPlayersReducer,
  form: sendFormReducer,
  rankings: getRankingsReducer,
  links: getLinksReducer,
  unique: getOnePlayerReducer,
});
