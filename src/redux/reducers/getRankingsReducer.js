const defaultState = {
  rankingsArrayProp: [],
};

export const getRankingsReducer = (state = defaultState, action) => {
  switch (action.type) {
    case 'GET_RANKINGS':
      console.log('Made it to reducer');
      return {
        ...state,
        rankingsArrayProp: action.payload,
      };
    default: return state;
  }
};
