const defaultState = {
  previousSentForm: [],
};

export const sendFormReducer = (state = defaultState, action) => {
  switch (action.type) {
    case 'SEND_FORM':
      console.log('Made it to reducer');
      return {
        ...state,
        previousSentForm: action.payload,
      };
    default: return state;
  }
};
