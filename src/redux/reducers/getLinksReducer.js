const defaultState = {
  linksArrayProp: [],
};

export const getLinksReducer = (state = defaultState, action) => {
  switch (action.type) {
    case 'GET_LINKS':
      console.log('Made it to reducer');
      return {
        ...state,
        linksArrayProp: action.payload,
      };
    default: return state;
  }
};
