const defaultState = {
  playersArrayProp: [],
};

export const getPlayersReducer = (state = defaultState, action) => {
  switch (action.type) {
    case 'GET_PLAYERS':
      console.log('Made it to reducer');
      return {
        ...state,
        playersArrayProp: action.payload,
      };
    default: return state;
  }
};
