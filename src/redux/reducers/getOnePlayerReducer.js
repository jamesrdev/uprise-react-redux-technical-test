const defaultState = {
  uniquePlayerArrayProp: [],
};

export const getOnePlayerReducer = (state = defaultState, action) => {
  switch (action.type) {
    case 'GET_ONE_PLAYER':
      console.log('Made it to reducer');
      return {
        ...state,
        uniquePlayerArrayProp: action.payload,
      };
    default: return state;
  }
};
