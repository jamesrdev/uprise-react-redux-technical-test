const handleDatabase = require('./handleDatabase');
const createPlayers = require('./createPlayers');
var webpackDevMiddleware = require("webpack-dev-middleware");
var webpackHotMiddleware = require("webpack-hot-middleware");
const express = require('express');
const webpack = require('webpack');
const app = express();
const config = require('../webpack.config.js');
const compiler = webpack(config);
const path = require("path");
const MongoClient = require('mongodb').MongoClient
const dotenv = require('dotenv');
const bodyParser = require('body-parser');

/*Initial setup---------------------------------------------------------*/
let links = [];
let myPlayers 

dotenv.config()

app.use(bodyParser.json());

app.use(
  webpackDevMiddleware(compiler, {
    hot: true,
    filename: "bundle.js",
    publicPath: "/",
    stats: {
      colors: true
    },
    historyApiFallback: true
  })
);
  
app.use(
  webpackHotMiddleware(compiler, {
    log: console.log,
    path: "/__webpack_hmr",
    heartbeat: 10 * 1000
  })
);

/*Routes------------------------------------------------------------------*/
app.get('/playerlist', (req, res) => {
  console.log('Server receieved data request from client')
  db.collection("scores").find().toArray(function(err, results) {
    if (results.length > 0) {
      //found scores, time to sort
      db.collection('scores').find().sort({index: 1}).toArray(function(err, results) {
        myPlayers = results.map((v) => {
          //console.log(v.name)
          return v.name
        })

        let unsort = myPlayers[0]
        myPlayers.shift()
        myPlayers.sort()
        myPlayers.unshift(unsort)
        res.json(myPlayers)
        //console.log(myPlayers)
      })        
    }
  })
})

app.get('/links', (req, res) => {
  db.collection('scores').find({ link : { $exists : true } }).toArray(function(err, results) {
    links = results.map((current, index) => {
      if (typeof current !== 'undefined') {
        return (
          current.link
        )		  
      }
    });
    res.json(links)
  });
})

app.get('/rankings', (req, res) => {
  db.collection("scores").find({ link: { $exists: true } }).sort({name: 1}).toArray(function(err, results) {
    res.json(results)
    console.log(results)
  })
})

app.post('/sendForm', (req, res) => {
  let myCallBack = (a = res) => a.json("Got your form, update your names")
  console.log(req.body)
  handleDatabase(req.body, myCallBack)
});

app.post('/player', (req, res) => {
  console.log('server got your link')
  console.log(req.body)
  db.collection('scores').find({ link: req.body.link }).toArray(function(err, results) {
    res.json(results)
  })
})


/*Connect to Database and listen-------------------------------------------------------------*/
MongoClient.connect(process.env.MONGO_CREDENTIALS, { useNewUrlParser: true }, (err, client) => {
  if (err) return console.log(err);
  db = client.db('stage8');

  //Set up links as server-side routes
  db.collection('scores').find({ link : { $exists : true } }).toArray(function(err, results) {      
    links = results.map((current, index) => {
      if (typeof current !== 'undefined') {
        return (
          current.link
          )		  
      }
    });

    ///Webpack dev middleware hack to make routes work
    app.use(['/', '/submit', ...[links]], function (req, res, next) {
      var filename = path.join(compiler.outputPath,'index.html');
      compiler.outputFileSystem.readFile(filename, function(err, result){
        if (err) {
          return next(err);
        }
        res.set('content-type','text/html');
        res.send(result);
        res.end();
      });
    });    
  });
      
  //Get Player names for drop downs
  db.collection("scores").find().toArray(function(err, results) {
    if (results.length > 0) {
      //found scores, time to sort
      console.log('found existing users')
      db.collection('scores').find().sort({index: 1}).toArray(function(err, results) {
        myPlayers = results.map((v) => {
          return v.name
        })
        let unsort = myPlayers[0]
        myPlayers.shift()
        myPlayers.sort()
        myPlayers.unshift(unsort)       
      })     
    } else {
      createPlayers()
    };			
  });

   //Required to make Heroku Apps work
  app.listen(process.env.PORT || 3000)
});

