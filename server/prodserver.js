const handleDatabase = require('./handleDatabase');
const createPlayers = require('./createPlayers');
const express = require('express');
const app = express();
const path = require("path");
const MongoClient = require('mongodb').MongoClient
const dotenv = require('dotenv');
const bodyParser = require('body-parser');
const appRoot = require('app-root-path');
var expressStaticGzip = require('express-static-gzip');

/*Initial setup---------------------------------------------------------*/
let links = [];
let myPlayers 

dotenv.config()

app.use(bodyParser.json());

app.use('/', expressStaticGzip(appRoot + ('/dist'), {
  enableBrotli: true
 }));

/*Routes------------------------------------------------------------------*/
app.get('/playerlist', (req, res) => {
  console.log('Server receieved data request from client')
  db.collection("scores").find().toArray(function(err, results) {
    if (results.length > 0) {
      //found scores, time to sort
      db.collection('scores').find().sort({index: 1}).toArray(function(err, results) {
        myPlayers = results.map((v) => {
          //console.log(v.name)
          return v.name
        })

        let unsort = myPlayers[0]
        myPlayers.shift()
        myPlayers.sort()
        myPlayers.unshift(unsort)
        res.json(myPlayers)
        //console.log(myPlayers)
      })        
    }
  })
})

app.get('/links', (req, res) => {
  db.collection('scores').find({ link : { $exists : true } }).toArray(function(err, results) {
    links = results.map((current, index) => {
      if (typeof current !== 'undefined') {
        return (
          current.link
        )		  
      }
    });
    res.json(links)
  });
})

app.get('/rankings', (req, res) => {
  db.collection("scores").find({ link: { $exists: true } }).sort({name: 1}).toArray(function(err, results) {
    res.json(results)
    console.log(results)
  })
})

app.post('/sendForm', (req, res) => {
  let myCallBack = (a = res) => a.json("Got your form, update your names")
  console.log(req.body)
  handleDatabase(req.body, myCallBack)
});

app.post('/player', (req, res) => {
  console.log('server got your link')
  console.log(req.body)
  db.collection('scores').find({ link: req.body.link }).toArray(function(err, results) {
    res.json(results)
  })
})


/*Connect to Database and listen-------------------------------------------------------------*/
MongoClient.connect(process.env.MONGO_CREDENTIALS, { useNewUrlParser: true }, (err, client) => {
  if (err) return console.log(err);
  db = client.db('stage8');

  //Set up links as server-side routes
  db.collection('scores').find({ link : { $exists : true } }).toArray(function(err, results) {      
    links = results.map((current, index) => {
      if (typeof current !== 'undefined') {
        return (
          current.link
          )		  
      }
    });


  app.get(['/', '/submit',...links], function(req, res) {
    res.sendFile(path.join(appRoot + '/dist/index.html'), function(err) {
    if (err) {
      res.status(500).send(err)
    }
    })
  })

      
  //Get Player names for drop downs
  db.collection("scores").find().toArray(function(err, results) {
    if (results.length > 0) {
      //found scores, time to sort
      console.log('found existing users')
      db.collection('scores').find().sort({index: 1}).toArray(function(err, results) {
        myPlayers = results.map((v) => {
          return v.name
        })
        let unsort = myPlayers[0]
        myPlayers.shift()
        myPlayers.sort()
        myPlayers.unshift(unsort)       
      })     
    } else {
      createPlayers()
    };			
  });

  //Required to make Heroku Apps work
  app.listen(process.env.PORT || 3000)
  });
})
