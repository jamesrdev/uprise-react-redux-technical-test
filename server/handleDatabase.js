


module.exports = function(a, b) {
    let mockObject =  a
    let handle1v1 = () => {
        let playerWinner
        let opponentWinner
        
        if (mockObject.players[0][1] == mockObject.winner) {
            playerWinner = true
        } else {
            playerWinner = false
        }

        if (mockObject.players[1][1] == mockObject.winner) {
            opponentWinner = true
        } else {
            opponentWinner = false
        }

        let player = `${mockObject.players[0][0]}`
        let opponent = `${mockObject.players[1][0]}`
        let mode = `${mockObject.mode}`
        let query = `${player}.${mode}.${opponent}.games`
        let opponentQuery = `${opponent}.${mode}.${player}.games`     
        
        db.collection("scores").update(
            {"name": player},
            { $push: { [query]: {"date": mockObject.date, "win": playerWinner}}})

                
        db.collection("scores").update(
            {"name": opponent},
            { $push: { [opponentQuery]: {"date": mockObject.date, "win": opponentWinner}}})
    }
  
    let handle2v1 = () => {
        mockObject.players.sort()
       let team1 = []
       let team2 = []
       let team1Win 
       let team2Win 
       mockObject.players.forEach((v,i) => {
         if (v[1] == "Team 1") {
           team1.push(v[0])
         } else {
           team2.push(v[0])
         }
       })
       team1 = team1.sort()
       team2 = team2.sort()
       console.log(team1)
       console.log(team2)
       if (mockObject.winner == "Team 1") {
         team1Win = true
         team2Win = false
       } else {
         team1Win = false
         team2Win = true
       }
  
       let opponentNameString = `${team1[0]} + ${team1[1]}`
       let player1String = `${team1[0]} + ${team1[1]}`
       let player2String = `${team1[1]} + ${team1[0]}`
       let opponentQuery = `${team2[0]}.${"1v2"}.${opponentNameString}.games`
       let player1Query = `${team1[0]}.${"2v1"}.${player1String}.${team2[0]}.games`
       let player2Query = `${team1[1]}.${"2v1"}.${player2String}.${team2[0]}.games`
       
       db.collection("scores").update(
         {"name": team2[0]},
         { $push: { [opponentQuery]: {"date": mockObject.date, "win": team2Win}}})
  
  
       db.collection("scores").update(
         {"name": team1[0]},
         { $push: { [player1Query]: {"date": mockObject.date, "win": team1Win}}})
  
  
       db.collection("scores").update(
         {"name": team1[1]},
         { $push: { [player2Query]: {"date": mockObject.date, "win": team1Win}}})
    }
  
    let handle1v2 = () => {
      let team1 = []
      let team2 = []
      mockObject.players.forEach((v,i) => {
        if (v[1] == "Team 1") {
          team1.push(v[0])
        } else {
          team2.push(v[0])
        }
      })
      team1 = team1.sort()
      team2 = team2.sort()
      let team1Winner 
      let team2Winner
      if (mockObject.winner == "Team 1") {
        team1Winner = true
        team2Winner = false
      } else {
        team1Winner = false
        team2Winner = true
      }
  
     
      let playerString = `${team2[0]} + ${team2[1]}`
      let opponent1String = `${team2[0]} + ${team2[1]}`
      let opponent2String = `${team2[1]} + ${team2[0]}`
      let playerQuery = `${team1[0]}.${"1v2"}.${playerString}.games`
      let opponent1Query = `${team2[0]}.${"2v1"}.${opponent1String}.${team1[0]}.games`
      let opponent2Query = `${team2[1]}.${"2v1"}.${opponent2String}.${team1[0]}.games`
      
      db.collection("scores").update(
        {"name": team1[0]},
        { $push: { [playerQuery]: {"date": mockObject.date, "win": team1Winner}}})
  
         
      db.collection("scores").update(
        {"name": team2[0]},
        { $push: { [opponent1Query]: {"date": mockObject.date, "win": team2Winner}}})
  
        db.collection("scores").update(
          {"name": team2[1]},
          { $push: { [opponent2Query]: {"date": mockObject.date, "win": team2Winner}}})
    } 
  
    let handle2v2 = () => {
      let team1 = []
      let team2 = []
      mockObject.players.forEach((v,i) => {
        if (v[1] == "Team 1") {
          team1.push(v[0])
        } else {
          team2.push(v[0])
        }
      })
      team1 = team1.sort()
      team2 = team2.sort()
      let team1Winner 
      let team2Winner
      if (mockObject.winner == "Team 1") {
        team1Winner = true
        team2Winner = false
      } else {
        team1Winner = false
        team2Winner = true
      }
  
      
      let player1String = `${team1[0]} + ${team1[1]}`
      let player2String = `${team1[1]} + ${team1[0]}`
      let opponent1String = `${team2[0]} + ${team2[1]}`
      let opponent2String = `${team2[1]} + ${team2[0]}`
  
      let opponentString = `${team2[0]} + ${team2[1]}`
      let reverseOpponentString = `${team1[0]} + ${team1[1]}`
      let player1Query = `${team1[0]}.${"2v2"}.${player1String}.${opponentString}.games`
      let player2Query = `${team1[1]}.${"2v2"}.${player2String}.${opponentString}.games`
      let opponent1Query = `${team2[0]}.${"2v2"}.${opponent1String}.${reverseOpponentString}.games`
      let opponent2Query = `${team2[1]}.${"2v2"}.${opponent2String}.${reverseOpponentString}.games`
   
    
      db.collection("scores").update(
        {"name": team1[0]},
        { $push: { [player1Query]: {"date": mockObject.date, "win": team1Winner}}})
  
      db.collection("scores").update(
        {"name": team1[1]},
        { $push: { [player2Query]: {"date": mockObject.date, "win": team1Winner}}})
  
      db.collection("scores").update(
        {"name": team2[0]},
        { $push: { [opponent1Query]: {"date": mockObject.date, "win": team2Winner}}})
  
      db.collection("scores").update(
        {"name": team2[1]},
        { $push: { [opponent2Query]: {"date": mockObject.date, "win": team2Winner}}})
    }
  
    
    
    var processCount = 0
    mockObject.players.forEach((v, i) => {
        db.collection("scores").find({name: v[0]}).toArray(function(err, results) {
          if (results.length > 0) {
              console.log('That player exists.')
              processCount++
              //handle players existing and not existing then run update code
              if (processCount == mockObject.players.length) {
                b()
                if (mockObject.mode == "1v1") {
                  handle1v1()
                } else if (mockObject.mode == "2v1") {
                handle2v1()
                } else if (mockObject.mode == "1v2") {
                  handle1v2()
                } else if (mockObject.mode == "2v2") {
                  handle2v2()
                }
              }
          
          } else {
              //create player of it doesnt exist then update scores in its callback
              db.collection("scores").count().then((count) => {
                formatLink = v[0].replace(/\s+/g, '-').toLowerCase()
                formatLink = "/" + formatLink
                db.collection('scores').save({ 
                  index: count,
                  name: v[0],
                  link: formatLink,
                  [v[0]]: {
                  ["1v1"]: {},            
                  ["2v1"]: {},
                  ["1v2"]: {},
                  ["2v2"]: {}
                  }
              }, () => {
                console.log('created it')
                  processCount++
                  if (processCount == mockObject.players.length) {
                    console.log('')
                    b()
                    if (mockObject.mode == "1v1") {
                    handle1v1()
                    } else if (mockObject.mode == "2v1") {
                    handle2v1()
                    } else if (mockObject.mode == "1v2") {
                    handle1v2()
                    } else if (mockObject.mode == "2v2") {
                    handle2v2()
                    }
                  }
                });
              })   

          }
        
        })
    })  
}

