module.exports = function() {
    //no scores, let's create 4
    console.log('no players found user, creating four new players');
    let formatLink
    db.collection('scores').save( { 
        index: 0,
        name: "Clear Selection"
    } );


    formatLink = "Player 1".replace(/\s+/g, '-').toLowerCase()
    formatLink = "/" + formatLink
    db.collection('scores').save( { 
        index: 1,
        name: "Player 1",
        link: formatLink,
        ["Player 1"]: {
        ["1v1"]: {
            ["Player 2"]: {
            games: [
                {
                date: "2019-4-15",
                win: false
                },
                {
                date: "2019-4-16",
                win: true
                },

            ]
            }
        },            
        ["2v1"]: {
            ["Player 1 + Player 2"]: {
            ["Player 3"]: {
                games: [
                {
                    date: "2019-3-1",
                    win: true
                }
                ]
            }
            }
        },
        ["1v2"]: {},
        ["2v2"]: {
            ["Player 1 + Player 2"]: {
            ["Player 3 + Player 4"]: {
                games: [
                {
                    date: "2019-4-13",
                    win: true
                }
                ]
            }
            }
        }
        }
    } );

    formatLink = "Player 2".replace(/\s+/g, '-').toLowerCase()
    formatLink = "/" + formatLink
    db.collection('scores').save( { 
        index: 2,
        name: "Player 2",
        link: formatLink,
        ["Player 2"]: {
        ["1v1"]: {
            ["Player 1"]: {
            games: [
                {
                date: "2019-4-15",
                win: true
                },
                {
                date: "2019-4-16",
                win: true
                }
            ]
            }
        },
        ["2v1"]: {},
        ["1v2"]: {},
        ["2v2"]: {
            ["Player 2 + Player 1"]: {
            ["Player 3 + Player 4"]: {
                games: [
                {
                    date: "2019-4-13",
                    win: true
                }
                ]
            }
            }
        }
        }
    } )

    formatLink = "Player 3".replace(/\s+/g, '-').toLowerCase()
    formatLink = "/" + formatLink
    db.collection('scores').save( { 
        index: 3,
        name: "Player 3",
        link: formatLink,
        ["Player 3"]: {
        ["1v1"]: {
            ["Player 4"]: {
            games: [
                {
                date: "2019-4-18",
                win: true
                }
            ]
            }
        },
        ["2v1"]: {
        },
        ["1v2"]: {
            ["Player 1 + Player 2"]: {
            games: [
                {
                date: "2019-3-1",
                win: false
                }
            ]
            }
        },
        ["2v2"]: {
            ["Player 3 + Player 4"]: {
            ["Player 2 + Player 1"]: {
                games: [
                {
                    date: "2019-4-13",
                    win: false
                }
                ]
            }
            }
        }
        }
    } );

    formatLink = "Player 4".replace(/\s+/g, '-').toLowerCase()
    formatLink = "/" + formatLink
    db.collection('scores').save( { 
        index: 4,
        name: "Player 4",
        link: formatLink,
        ["Player 4"]: {
        ["1v1"]: {
            ["Player 3"]: {
            games: [
                {
                date: "2019-4-18",
                win: false
                }
            ]
            }
        },
        ["2v1"]: {
        },
        ["1v2"]: {
            ["Player 1 + Player 2"]: {
            games: [
                {
                date: "2019-3-1",
                win: false
                }
            ]
            }
        },
        ["2v2"]: {
            ["Player 4 + Player 3"]: {
            ["Player 2 + Player 1"]: {
                games: [
                {
                    date: "2019-4-13",
                    win: false
                }
                ]
            }
            }
        }
        }
    } );
}